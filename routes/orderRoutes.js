const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.post("/checkout", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);
    let data = {
        userId : userData.id,
        item: req.body
    };

    orderController.checkoutProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get("/viewOrders", auth.isAdminVerify, (req, res) => {
    orderController.viewAllOrders().then(resultFromController => res.send(resultFromController));
});

router.get("/viewOrders", auth.verify, (req, res) => {
    let data = auth.decode(req.headers.authorization);

    orderController.viewUserOrders(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;