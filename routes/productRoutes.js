const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

router.post("/addProduct", auth.isAdminVerify, (req, res) => {
    productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/viewAllProducts", auth.isAdminVerify, (req, res) => {
    productController.viewAllProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/viewActiveProducts", (req, res) => {
    productController.viewActiveProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/view/:id", (req, res) => {
    productController.viewProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/archive/:id", auth.isAdminVerify, (req, res) => {
    productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/activate/:id", auth.isAdminVerify, (req, res) => {
    productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/edit/:id", auth.isAdminVerify, (req, res) => {
    const data = {
        prodId: req.params.id,
        prodDet: req.body
    };
    
    productController.editProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;