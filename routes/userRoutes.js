const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/register", (req, res) => {
    userController.register(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
    userController.login(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/viewAllUsers", (req, res) => {
    userController.viewAllUsers().then(resultFromController => res.send(resultFromController));
});

router.get("/profile", auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization);
    
    userController.viewProfile(userData).then(resultFromController => res.send(resultFromController));
});

router.put("/changeToAdmin", auth.isAdminVerify, (req, res) => {
    userController.setToAdmin(req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;