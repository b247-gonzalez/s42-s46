const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name must be provided"]
    },
    lastName : {
        type : String,
        required : [true, "Last name must be provided"]
    },
    email : {
        type : String,
        required : [true, "Email must be provided"]
    },
    mobileNumber : {
        type : String,
        required : [true, "Mobile number must be provided"]
    },
    password : {
        type : String,
        required : [true, "Password must be provided"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    orders : [{
        orderId : {
            type : String,
            required : [true, "Order ID must be provided"]
        },
        orderedOn : {
            type : Date,
            default : new Date()
        },
        status : {
            type : String,
            default : "Pending"
        }
    }],
    createdOn : {
        type : Date,
        default : new Date()
    }
});

module.exports = mongoose.model("User", userSchema);