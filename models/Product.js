const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "Product name must be provided."]
    },
    description : {
        type : String,
        required : [true, "Product description must be provided"]
    },
    price : {
        type : Number,
        required : [true, "Product price must be provided"]
    },
    availableStocks : {
        type : Number,
        default : 1
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : new Date()
    }
});

module.exports = mongoose.model("Product", productSchema);