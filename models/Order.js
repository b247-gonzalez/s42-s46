const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId : {
        type : String,
        required : [true, "User ID must be provided"]
    },
    product : {
        type : String,
        required : [true, "Product name must be provided"]
    },
    total : {
        type : Number,
        required : [true, "Total price must be provided"]
    },
    isPaid : {
        type : String,
        default : false
    },
    createdOn : {
        type : Date,
        default : new Date()
    },
    status : {
        type : String,
        default : "Pending"
    }
});

module.exports = mongoose.model("Order", orderSchema);