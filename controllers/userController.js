const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.register = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNumber : reqBody.mobileNumber,
        password : bcrypt.hashSync(reqBody.password, 10),
    });
    
    return User.findOne({email : reqBody.email}).then(rslt => {
        if (rslt == null) {
            return newUser.save().then((user, err) => {
                if (err) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
        else {
            return "User with that email already exist!"
        }
    });
};

module.exports.login = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(rslt => {
        if (rslt == null) {
            return "User does not exist!"
        }
        else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, rslt.password);

            if (isPasswordCorrect) {
                return {
                    access : auth.createAccessToken(rslt)
                };
            }
            else {
                return "Incorrect password!";
            }
        }
    });
};

module.exports.viewAllUsers = () => {
    return User.find({}).then(rslt => {
        return rslt;
    });
};

module.exports.viewProfile = (data) => {
    return User.findById(data.id).then(rslt => {
        rslt.password = "";
        return rslt;
    });
};

module.exports.setToAdmin = (reqBody) => {
    let userChanged = {
        isAdmin: true
    }
    
    return User.findByIdAndUpdate(reqBody.id, userChanged).then((usr, err) => {
        if (err) {
            return err;
        }
        else {
            return true
        }
    });
};