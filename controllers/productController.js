const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addProduct = (reqBody) => {
    let newProduct = new Product({
        name : reqBody.name,
        description : reqBody.description,
        availableStocks : reqBody.availableStocks,
        price : reqBody.price
    });

    return newProduct.save().then((product, err) => {
        if (err) {
            return err;
        }
        else {
            return true;
        }
    });
};

module.exports.viewAllProducts = () => {
    return Product.find({}).then(rslt => {
        return rslt;
    });
};

module.exports.viewActiveProducts = () => {
    return Product.find({isActive: true}).then(rslt => {
        return rslt;
    });
};

module.exports.viewProduct = (reqParams) => {
    return Product.findById(reqParams.id).then(rslt => {
        return rslt;
    });
};

module.exports.archiveProduct = (reqParams) => {
    let archivedProduct = {
        isActive: false
    }

    return Product.findByIdAndUpdate(reqParams.id, archivedProduct).then((product, err) => {
        if (err) {
            return err;
        }
        else {
            return `${product.name} has been archived!`;
        }
    });
};

module.exports.activateProduct = (reqParams) => {
    let activatedProduct = {
        isActive: true
    }

    return Product.findByIdAndUpdate(reqParams.id, activatedProduct).then((product, err) => {
        if (err) {
            return err;
        }
        else {
            return `${product.name} has been activated!`;
        }
    });
};

module.exports.editProduct = (data) => {
    let updatedProduct = {
        name: data.prodDet.name,
        description: data.prodDet.description,
        price: data.prodDet.price,
        availableStocks: data.prodDet.availableStocks
    }
    
    return Product.findByIdAndUpdate(data.prodId, updatedProduct).then((prod, err) => {
        if (err) {
            return err;
        }
        else {
            return true;
        }
    });
};