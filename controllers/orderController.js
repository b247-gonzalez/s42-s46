const Product = require("../models/Product");
const Order = require("../models/Order");

module.exports.checkoutProduct = (data) => {
    let updatedProduct = {
        $inc: { availableStocks: -data.item.quantity }
    }
    return Product.findByIdAndUpdate(data.item.productId, updatedProduct).then(prod => {
        if (prod.availableStocks > data.item.quantity) {
            let subtotal = data.item.quantity * prod.price;
            let newOrder = new Order({
                userId: data.userId,
                product: prod.name,
                total: subtotal
            });

            return newOrder.save().then((order, err) => {
                if (err) {
                    return err;
                }
                else {
                    return true;
                }
            });
        }
        else {
            return false;
        }
    });
};

module.exports.viewUserOrders = (data) => {
    return Order.find({userId: data.id}).then(rslt => {
        return rslt;
    });
};

module.exports.viewAllOrders = () => {
    return Order.find({}).then(rslt => {
        return rslt;
    });
};